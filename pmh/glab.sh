#!/bin/sh

GLAB="${GLAB:=glab}"

set -e

if [ -z "$1" ]; then
  echo "Usage: glab.sh <milestone> [<milestone>]..."
  exit 1
fi

for MILESTONE in $*; do

  echo "## Issues with milestone $MILESTONE set but no workflow:: labels:"
  "$GLAB" -R gitlab-org/gitaly issue list -F urls --per-page 999 --milestone "$MILESTONE" \
    --not-label "workflow::planning breakdown,workflow::ready for development,workflow::in dev,workflow::ready for review,workflow::in review" |\
    egrep -v "(^Showing|^$)" |\
    sed 's/^/- [ ] /'  # for Markdown formatting

done

echo "## Issues with no milestone but workflow:: labels:"

for label in   "workflow::ready for development" "workflow::in dev" "workflow::ready for review" "workflow::in review"; do
  echo "### ~\"$label\""
  "$GLAB" -R gitlab-org/gitaly issue list -F urls --per-page 999 --milestone none --label "$label" |\
    egrep -v "(^Showing|^$)" |\
    sed 's/^/- [ ] /'  # for Markdown formatting

done
